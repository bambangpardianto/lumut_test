<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends BP_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->tbl = 'account';
		$this->url_ = 'akun';
		$this->pk = 'id';
		$this->dis_type = ['id'];
		$this->type = ['username'=>'text',
						'password'=>'text',
						'name'=>'text',
						'role'=>'select'];
	}
}
