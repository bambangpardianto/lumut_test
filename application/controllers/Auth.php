<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function login()
	{
		$u = $this->input->post('username');
		$p = $this->input->post('password');

		//$hash = md5($p);

		$q = $this->db->get_where('akun',['username'=>$u, 'password'=>$q])->result();
		if ($q > 1) {
			$array = array(
				'hak_akses' => ''
			);
			
			$this->session->set_userdata( $array );
			redirect('post','refresh');
		} else {
			redirect('','refresh');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('','refresh');
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */ ?>