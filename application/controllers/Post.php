<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends BP_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->tbl = 'post';
		$this->url_ = 'post';
		$this->pk = 'idpost';
		$this->dis_type = ['idpost'];
		$this->type = ['title' => 'text', 'content'=>'text','date'=>'date','username'=>'text'];
	}

}

/* End of file Post.php */
/* Location: ./application/controllers/Post.php */ ?>