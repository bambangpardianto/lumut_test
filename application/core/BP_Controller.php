<?php 
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class BP_Controller extends CI_Controller {
		
		protected $tbl 				= null;
		protected $url_ 			= null;
		protected $pk 				= 'id';
		protected $dis_type 		= ['id'];
		protected $data_table 		= true;
		protected $ren_col 			= [];
		protected $read_joined 		= [];
		protected $cmb_d 			= [];
		protected $type 			= [];
		protected $ren_label 		= [];

		public function __construct()
		{
			parent::__construct();
			$this->load->library('bambangcrud');
		}
	 
		/*
		| List all your items
		*/
		public function index()
		{
			$id 		= $this->pk;
			$dis_col 	= $this->dis_type;
			$rename_col = $this->ren_col;
			$join 		= $this->read_joined;
			$dt 		= $this->data_table; 

			$data['contents'] = $this->bambangcrud->list_th($this->tbl,$id,$join,$dis_col,$rename_col,$dt);
			$this->load->view('template',$data);
		}

		/*
		| Read one item
		*/
		public function detail( $id = null )
		{
			$opsi = [
				'updt' => true,
				'submit' => $this->uri->segment(2),
			    'aksi' => $this->url_,
			    'metode' => 'post',
			    'c' => 12,
			    'type' => $this->arr_tipe(),
			    'dis_type' => $this->dis_type,
			    'rename_label' => $this->ren_label,
			];

			$cmb_dinamic = $this->cmb_d;
			$dt = $this->db->where($this->pk,$id)->get($this->tbl)->result()[0];
			//$join = [$this->tbl_joined,$this->field_joined,$this->pk];
			$data['contents'] = $this->bambangcrud->form_dinamis($this->tbl,$opsi,$dt,$id,$cmb_dinamic);
			$this->load->view('template', $data);
		}
	
		/*
		| Add a new item
		*/
		public function tambah()
		{
			$id = '';
			$opsi = [
				'updt' => false,
				'submit' => 'Simpan',//$this->uri->segment(2),
			    'aksi' => $this->url_.'/act_tambah',
			    'metode' => 'post',
			    'c' => 12,
			    'type' => $this->arr_tipe(),
			    'dis_type' => $this->dis_type,
			    'rename_label' => $this->ren_label,
			];

			$cmb_dinamic = $this->cmb_d;
			$dt = [];
			//$join = [$this->tbl_joined,$this->field_joined,$this->pk];
			$data['contents'] = $this->bambangcrud->form_dinamis($this->tbl,$opsi,$dt,$id,$cmb_dinamic);
			$this->load->view('template', $data);
		}

		/*
		| Action Add a new Item
		*/
		public function act_tambah()
		{
			$list_column = $this->bambangcrud->list_colomdb($this->tbl);
			$data_post = $this->input->post();array_pop($data_post);
			$data = array_combine($list_column, $data_post);

			//print_r($this->cmb_d);die();
			$query = $this->db->insert($this->tbl,$data);
			if ($query) {
				$this->session->set_flashdata('message', 'Berhasil Input Data');
				redirect($this->url_,'refresh');
			} else {
				$this->session->set_flashdata('message', 'Gagal Input Data');
				redirect($this->url_,'refresh');
			}
		}
	
		/*
		| Update a new item
		*/
		public function edit( $id = null )
		{
			$opsi = [
				'updt' => true,
				'submit' => 'Update',//$this->uri->segment(2),
			    'aksi' => $this->url_.'/act_edit',
			    'metode' => 'post',
			    'c' => 12,
			    'type' => $this->arr_tipe(),
			    'dis_type' => $this->dis_type,
			    'rename_label' => $this->ren_label,
			];
			
			$cmb_dinamic = $this->cmb_d;
			$dt = $this->db->where($this->pk,$id)->get($this->tbl)->result()[0];
			//$join = [$this->tbl_joined,$this->field_joined,$this->pk];
			$data['contents'] = $this->bambangcrud->form_dinamis($this->tbl,$opsi,$dt,$id,$cmb_dinamic);
			$this->load->view('template', $data);
		}

		/*
		| Action update a new Item
		*/
		public function act_edit()
		{
			$list_column = $this->bambangcrud->list_colomdb($this->tbl);
			$data_post = $this->input->post();array_pop($data_post);
			$data = array_combine($list_column, $data_post);array_shift($data);
			
			//print_r($data);die();
			$query = $this->db->where($this->pk,$this->input->post($this->pk))->update($this->tbl,$data);
			if ($query) {
				$this->session->set_flashdata('message', 'Berhasil Update Data');
				redirect($this->url_,'refresh');
			} else {
				$this->session->set_flashdata('message', 'Gagal Update Data');
				redirect($this->url_,'refresh');
			}
		}
	
		/*
		|Delete one item
		*/
		public function delete( $id = NULL )
		{
			$row = $this->db->where($this->pk,$id)
							->delete($this->tbl);
			if ($row) {
	            $this->session->set_flashdata('message', 'Hapus Data Berhasil');
	            redirect(site_url($this->url_));
	        } else {
	            $this->session->set_flashdata('message', 'Hapus Data Gagal');
	            redirect(site_url($this->url_));
	        }
		}


		/*
		| Generate Field disable when $type is fill on declaration variable on top.
		*/
		protected function arr_tipe()
		{
			$data = [];
			$q_ = $this->bambangcrud->list_colomdb($this->tbl);
			if (empty($this->type)) {
				foreach ($q_ as $v_) {
					$data[$v_] = "text";
				}
			} else {
				$data = $this->type;
			}
			return $data;
		}
	
	}
	
	/* End of file Bp_Controller.php */
	/* Location: ./application/core/Bp_Controller.php */
 ?>