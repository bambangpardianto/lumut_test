<?php 
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class bambangcrud
	{
		protected $ci;

		public function __construct()
		{
			$this->ci =& get_instance();
		}

		public function cmb_dinamis($name,$table,$field,$pk,$selected=null){
		    
		    $cmb = "<select name='$name' class='form-control'>"."<option value=''>--Silakan Pilih--</option>";
		    $data = $this->ci->db->get($table)->result();
		    foreach ($data as $d){
		        $cmb .="<option value='".$d->$pk."'";
		        $cmb .= $selected==$d->$pk?" selected='selected'":'';
		        $cmb .=">". strtoupper($d->$field) ."</option>";
		    }
		    $cmb .="</select>";
		    return $cmb;  
		}

		public function cmb_dinamis_user($name,$table,$field,$pk,$selected=null){
		    
		    $cmb = "<select name='$name' class='form-control'>"."<option value=''>--Silakan Pilih--</option>";
		    $data = $this->ci->db->get($table)->result();
		    foreach ($data as $d){
		        $cmb .="<option value='".$d->$pk."'";
		        $cmb .= $selected==$d->$pk?" selected='selected'":'';
		        $cmb .=">". getUserById( strtoupper($d->$field) )."</option>";
		    }
		    $cmb .="</select>";
		    return $cmb;  
		}

		public function cmb_dinamis_group($name,$table,$field,$pk,$selected=null){
		    
		    $cmb = "<select name='$name' class='form-control'>"."<option value=''>--Silakan Pilih--</option>";
		    $data = $this->ci->db->get($table)->result();
		    foreach ($data as $d){
		        $cmb .="<option value='".$d->$pk."'";
		        $cmb .= $selected==$d->$pk?" selected='selected'":'';
		        $cmb .=">". getGroupById( strtoupper($d->$field) )."</option>";
		    }
		    $cmb .="</select>";
		    return $cmb;  
		}

		public function select2_dinamis($name,$table,$field,$placeholder){
		    
		    $select2 = '<select name="'.$name.'" class="form-control select2 select2-hidden-accessible" multiple="" 
		               data-placeholder="'.$placeholder.'" style="width: 100%;" tabindex="-1" aria-hidden="true">';
		    $data = $this->ci->db->get($table)->result();
		    foreach ($data as $row){
		        $select2.= ' <option>'.$row->$field.'</option>';
		    }
		    $select2 .='</select>';
		    return $select2;
		}

		public function datalist_dinamis($name,$table,$field,$value=null){
		    
		    $string = '<input value="'.$value.'" name="'.$name.'" list="'.$name.'" class="form-control">
		    <datalist id="'.$name.'">';
		    $data = $this->ci->db->get($table)->result();
		    foreach ($data as $row){
		        $string.='<option value="'.$row->$field.'">';
		    }
		    $string .='</datalist>';

		    return $string;
		}

		public function datalist_dinamis2($name,$table,$field,$field2,$value=null,$toForm){
		    
		    $string = '<input value="'.$value.'" name="'.$name.'" list="'.$name.'" class="form-control">
		    <datalist id="'.$name.'">';
		    $data = $this->ci->db->get($table)->result();
		    foreach ($data as $row){
		        $string.='<option data-value="'.$row->$field2.'" value="'.$row->$field.'">';
		    }
		    $string .='</datalist>';

		    $string .='<script>';
		    $string .='document.querySelector("input[list]").addEventListener("input", public function(e) {';
		    $string .='var input = e.target,';
		    $string .='list = input.getAttribute("list"),';
		    $string .='options = document.getElementById("'.$name.'").options,';
		    $string .='hiddenInput = document.getElementById("'.$toForm.'"),';
		    $string .='inputValue = input.value;';
		    $string .='hiddenInput.value = inputValue;';
		    $string .='for(let i = 0; i < options.length; i++) {';
		    $string .=' let option = document.getElementById("'.$name.'").options[i];';
		    $string .=' if(option.value === inputValue) {';
		    $string .=' hiddenInput.value = option.getAttribute("data-value");';
		    $string .=' break;';
		    $string .='}';
		    $string .='}';
		    $string .='});';
		    $string .='</script>';
		    return $string;
		}

		public function set_enum_val($table,$field1,$new_val)
		{
			$this->ci =& get_instance();$t='';
		    $query = $this->ci->db->query("SHOW COLUMNS FROM `{$table}` WHERE Field = '{$field1}'")->row(0)->Type;
		    preg_match("/^enum\(\'(.*)\'\)$/",$query,$matches);
		    $res_enum = explode("','", $matches[1]);
		    array_push($res_enum, $new_val);
		    foreach ($res_enum as $key => $value) {
		    	$t .= "'".$value."'".",";
		    }
		    $new = rtrim($t,', ');
			$q = $this->ci->db->query("
				ALTER TABLE
				    `$table`
				MODIFY COLUMN
				    `$field1` enum(".$new.")
			");
			return $q;
		}

		public function get_enum($name, $table, $field, $selected=null){
			$select = '';
		    $this->ci =& get_instance();
		    $query = $this->ci->db->query("SHOW COLUMNS FROM `{$table}` WHERE Field = '{$field}'")->row(0)->Type;
		    preg_match("/^enum\(\'(.*)\'\)$/",$query,$matches);
		    $pilihan = explode("','", $matches[1]);
		    $select .= '<select class="form-control" name="'.$name.'">';
		    $select .= ($selected==null)?'<option>--Pilihan--</option>':'<option value="'.$selected.'">'.$selected.'</option>';
		    foreach ($pilihan as $key => $value) {
		    	$select .= '<option value="'.$value.'">'.$value.'</option>';
		    }
		    $select .= '</select>';
		    return $select;
		}

		public function rename_string_is_aktif($string){
		        return $string=='y'?'Aktif':'Tidak Aktif';
		    }
		    
		public function getUserById($id){
		    
		    $query = $this->ci->db->get_where('users',['id' => $id])->row('username');
		    return $query;
		}

		public function getGroupById($id){
		    
		    $query = $this->ci->db->get_where('groups',['id' => $id])->row('name');
		    return $query;
		}


		public function getNama($id,$table,$field){
		    $this->ci =& get_instance();
		    $query = $this->ci->db->where('id',$id)->get($table)->row($field);
		    return $query;
		}

		public function alert($class,$title,$description){
		    return '<div class="alert '.$class.' alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                <h4><i class="icon fa fa-ban"></i> '.$title.'</h4>
		                '.$description.'
		              </div>';
		}

		public function autocomplate_json($table,$field){
		    
		    $this->ci->db->like($field, $_GET['term']);
		    $this->ci->db->select($field);
		    $collections = $this->ci->db->get($table)->result();
		    foreach ($collections as $collection) {
		        $return_arr[] = $collection->$field;
		    }
		    echo json_encode($return_arr);
		}

		/*
		| Method for upload file
		| just one file.
		| as soon as multi file.
		*/
		public function upload_file($data,$table,$field) {
		    $this->ci =& get_instance();
		    $config['upload_path']          = './uploads/images/';
		    $config['allowed_types']        = 'gif|jpg|png';
		    $config['max_size']             = 3000;
		    //$config['file_name']            = 'berita-acara';
		    //$config['max_width']            = 1024;
		    //$config['max_height']           = 768;

		    $this->ci->load->library('upload', $config);
		    
		    if ( ! $this->ci->upload->do_upload($field)){
		        $error = array('error' => $this->ci->upload->display_errors());
		        print_r($error);
		    }else{
		    	$file_ = [$field=>$this->upload->data()];

		        $query = $this->ci->db->insert($table, array_merge($data,$file_));

		        if ($query) {
		            $this->session->set_flashdata('message', 'success');
		            redirect($table,'refresh');
		        } else {
		            $this->session->set_flashdata('message', 'failed');
		            redirect($table,'refresh');
		        }
		    }
		}

		/*
		| Helper for generate input form from database
		| task is done :
		|   - input type varchar done.
		|   - setting input select/combobox, checkbox, radio, date, enum done.
		|   - input, update, detail form done.
		*/
		public function form_dinamis($table, $ops = null, $data = null, $id = null, $cmb_dinamic = null) {
		    $this->ci =& get_instance();
		    $col = [];$tgl = false;
		    $form = "<form class='form-horizontal' action=".site_url($ops['aksi'])." method='$ops[metode]' enctype='multipart/form-data'>";
		    $form .= "<input type='hidden' name='id' value='$id'>";
		    $query = $this->ci->db->query("describe `{$table}`")->result();
		    $form .= "<div class='col-md-$ops[c]' style='padding-bottom: 2rem;'>";
		    foreach ($query as $v) {
		        $col[$v->Field] = $v->Field;
		        if ($v->Field == "create") {
		            $tgl = true;
		        }
		    }
		    foreach ($query as $k => $row) {

		        $rf = ($data == null) ? "" : $data->{$row->Field};
		        
		        $name[] = $row->Field;
		        $r_name[$row->Field] = $row->Field;
		        $filter_col = array_diff($name, $ops['dis_type']);
		        $rename_col = array_intersect_key($ops['rename_label'], $r_name);

		        $readonly = ($this->ci->uri->segment(2) == "detail")?'readonly':'';
		        if (in_array($row->Field, $filter_col)) {
		            if ($ops['updt'] AND $data != null) {
		                $form .= "<div class='form-group'>";
		                if (in_array($row->Field, array_keys($rename_col))) {
		                	$form .= "<label for='".$rename_col[$row->Field]."' class='col-md-3 control-label'>".$rename_col[$row->Field]."</label>";
			            } else {
			            	$form .= "<label for='$row->Field' class='col-md-3 control-label'> " . str_replace('_', ' ', $row->Field) . "</label>";
			            }
		                

		                $enbl[] = array_intersect_key($ops['type'], $col);
		                foreach ($enbl as $clo) {
		                    if ($clo[$row->Field] == "select") { //AND $this->ci->uri->segment(2) != "detail") {
		                        $form .= "<div class='col-md-9'>";
		                        if ($cmb_dinamic == null) {
		                        	$form .= $this->get_enum($row->Field,$table,$row->Field,$rf);
		                        } else {
			                        foreach ($cmb_dinamic as $c_key => $cmb_di) {
			                        	if ($row->Field == $c_key) {
			                        		$form .= cmb_dinamis($row->Field,$cmb_di[0],$cmb_di[1],$cmb_di[2],$rf);
			                        		break;
			                        	}
			                    	}
		                    	}
		                        $form .= "</div>";
		                        break;
		                    } else if ($clo[$row->Field] == "radio") {
		                        $form .= "<div class='col-md-9'><input type='radio' name='$row->Field' class='form-control' value='$rf' ".$readonly."></div>";
		                        break;
		                    } 
		                    else if ($clo[$row->Field] == "checkbox") {
		                        $form .= "<div class='col-md-9'><input type='checkbox' name='$row->Field' class='form-control' value='$rf' ".$readonly."></div>";
		                        break;
		                    }
		                    else if ($clo[$row->Field] == "date") {
		                        $form .= "<div class='col-md-9'><input type='date' name='$row->Field' class='form-control' value='$rf' ".$readonly."></div>";
		                        break;
		                    } 
		                    else {
		                        $form .= "<div class='col-md-9'><input type='text' name='$row->Field' class='form-control' value='$rf' ".$readonly."></div>";
		                        break;
		                    }
		                }
		                   
		                $form .= "</div>";
		            } else {
		                $form .= "<div class='form-group'>";
		                                
		                if (in_array($row->Field, array_keys($rename_col))) {
		                	$form .= "<label for='".$rename_col[$row->Field]."' class='col-md-3 control-label'>".$rename_col[$row->Field]."</label>";
			            } else {
			            	$form .= "<label for='$row->Field' class='col-md-3 control-label'> " . str_replace('_', ' ', $row->Field) . "</label>";
			            }
			            
		                $enbl[] = array_intersect_key($ops['type'], $col);
		                foreach ($enbl as $clo) {
		                    if ($clo[$row->Field] == "select") {
		                        $form .= "<div class='col-md-9'>";
		                        if ($cmb_dinamic == null) {
		                        	$form .= $this->get_enum($row->Field,$table,$row->Field);
		                        } else {
			                        foreach ($cmb_dinamic as $c_key => $cmb_di) {
			                        	if ($row->Field == $c_key) {
			                        		$form .= $this->cmb_dinamis($row->Field,$cmb_di[0],$cmb_di[1],$cmb_di[2],$rf);
			                        		break;
			                        	}
			                    	}
		                    	}
		                        $form .= "</div>";
		                        break;
		                    } else if ($clo[$row->Field] == "radio") {
		                        $form .= "<div class='col-md-9'><input type='radio' name='$row->Field' class='form-control' value='$rf'></div>";
		                        break;
		                    } 
		                    else if ($clo[$row->Field] == "checkbox") {
		                        $form .= "<div class='col-md-9'><input type='checkbox' name='$row->Field' class='form-control' value='$rf'></div>";
		                        break;
		                    } 
		                    else if ($clo[$row->Field] == "date") {
		                        $form .= "<div class='col-md-9'><input type='date' name='$row->Field' class='form-control' value='$rf'></div>";
		                        break;
		                    }
		                    else if ($clo[$row->Field] == "file") {
		                        $form .= "<div class='col-md-9'><input type='file' name='$row->Field' class='form-control' value='$rf'></div>";
		                        break;
		                    }  
		                    else {
		                        $form .= "<div class='col-md-9'><input type='text' name='$row->Field' class='form-control' value='$rf'></div>";
		                        break;
		                    }
		                }
		                      
		                $form .= "</div>";
		            }
		        }
		    }
		    $form .= "<div class='form-group'><label class='col-md-3 control-label'></label>";
		    $form .= "<div class='col-md-9'>";
		    $form .= ($tgl)?"<input type='hidden' name='create' value=".date('Y-m-d').">":"";
		    $form .= "<a href='".$_SERVER['HTTP_REFERER']."' class='btn btn-default'>Kembali</a>";
		    $form .= "<span style='padding: 0px 3px 0px 3px;'></span>";
		    $form .= "<input type='submit' class='btn btn-success' value='$ops[submit]' name='submit'/>";
		    $form .= "</div></div>";
		    $form .= "</div></form>";
		    return $form;
		}

		/*
		| Helper for generate name field
		*/
		public function list_colomdb($table) {
		    $this->ci =& get_instance();
		    $data = [];
		    $query = $this->ci->db->query("describe `{$table}`")->result();
		    foreach ($query as $key => $val) {
		        $data[$val->Field] = $val->Field;
		    }
		    return $data;
		}

		/*
		| Helper for generate table from database
		| tools using boostrap 3, Jquery and DataTable
		| support costumize css with bootstrap 3.
		*/
		public function list_th($table,$_id,$join=null,$col=null,$_col=null,$data_table) {
			//$_id = is_array($_id)?$_id:$_id;
		    $this->ci =& get_instance();
		    $name = [];$kolom = [];$filter_col = [];$r_name = [];
		    $query = $this->ci->db->query("describe `{$table}`")->result();
		    $res = (is_array($_id))?$this->ci->db->where($_id)->get($table)->result():$this->ci->db->query("select * from `{$table}`")->result();
		    $start = 0;$a = 0;

		    $data = "<div class='container-fluid'>";
		    $data .= "<div class='pull-left'>".anchor(current_url().'/tambah', '<i class="fas fa-plus" aria-hidden="true"></i> Tambah Data', 'class="btn btn-primary btn-sm" style="margin-bottom: 1rem;"')."</div>";

		    /*Start Header If Not using datatable*/
		    if (!$data_table) {
		    $data .= "<div class='pull-right'>";
		    $data .= "<form action='site_url($table)' class='form-inline' method='get'>
		                    <div class='input-group'>
		                        <input type='text' class='form-control' name='q' value=''>
		                        <span class='input-group-btn'>
		                            
		                          <button class='btn btn-primary' type='submit'>Search</button>
		                        </span>
		                    </div>
		                </form></div>";
		    }
		    /*End Header If Not using datatable*/

		    /*Start Table*/
		    $data .= "<table class='table table-bordered table-responsive nowrap' id='$table' style='margin-top: 1rem;width:100%;background-color:white;display:block;'>";

		    /*Start Header Table*/
		    $data .= "<thead><tr>";
		    $data .= "<th>Aksi</th>";
		    foreach ($query as $val) {
		        $name[] = $val->Field;
		        $r_name[$val->Field] = $val->Field;
		        $filter_col = array_diff($name, $col);
		        $rename_col = array_intersect_key($_col, $r_name);

		        if (in_array($val->Field, $filter_col)) {
		            if (in_array($val->Field, array_keys($rename_col))) {
		                $data .= "<th>".$rename_col[$val->Field]."</th>";
		            } else {
		                $data .= "<th> " . str_replace('_', ' ', $val->Field) . "</th>";
		            }
		        }
		    }
		    $data .= "</tr></thead>";
		    /*End Header Table*/

		    /*Start Table If Not using datatable*/
		    if (!$data_table) {
		    $data .= "<tbody>";
			    foreach ($res as $k => $v) {
			        $data .= "<tr>";
			        //$data .= "<td>".++$start."</td>";
			        foreach ($v as $key => $value) {
			            if (in_array($key, $filter_col)) {

			                if ($join == null) {
			                    $data .= "<td>".$value."</td>";
			                } else {
			                    foreach ($join as $kj => $jj) {
			                        foreach ($jj[2] as $fs) {
			                            if ($fs != $key) {
			                                break;
			                            } else {
			                                $value = ($fs == $key)?$this->getNama($value,$jj[0],$jj[1]):$value;
			                            }
			                        }
			                    }
			                    $data .= "<td>".$value."</td>";
			                }
			            }
			        }
			        $data .= "<td>";
			        $data .= anchor(current_url().'/detail/'.$v->$_id, '<i class="fa fa-eye" aria-hidden="true"></i>', 'class="btn btn-info btn-sm"')."  ";
			        $data .= anchor(current_url().'/edit/'.$v->$_id, '<i class="far fa-edit" aria-hidden="true"></i>', 'class="btn btn-warning btn-sm"')."  ";
			        $data .= anchor(current_url().'/delete/'.$v->$_id, '<i class="fas fa-trash" aria-hidden="true"></i>', 'class="btn btn-danger btn-sm"');
			        $data .= "</td>";
			        $data .= "</tr>";
			    }
			}
			/*End Table If Not using datatable*/

			/*Start Footer Table*/
			$data .= "<tfoot><tr>";
			$data .= "<th>Aksi</th>";
		    foreach ($query as $val) {
		        $name[] = $val->Field;
		        $r_name[$val->Field] = $val->Field;
		        $filter_col = array_diff($name, $col);
		        $rename_col = array_intersect_key($_col, $r_name);

		        if (in_array($val->Field, $filter_col)) {
		            if (in_array($val->Field, array_keys($rename_col))) {
		                $data .= "<th>".$rename_col[$val->Field]."</th>";
		            } else {
		                $data .= "<th>".$val->Field."</th>";
		            }
		        }
		    }
		    $data .= "</tr></tfoot>";
		    /*End Footer Table*/

		    $data .= "</table></div>";
		    /*End Table*/

		    if ($data_table) {
		        $data .= "<script src='".base_url('assets/js/plugins/dataTables/datatables.min.js')."'></script>";
		        $data .= "<script>";
		        
		        $data .= "$(document).ready(function() {";
		        $data .= "$('#".$table." tfoot th').each( function () {";
				$data .= "var title = $(this).text();";
				$data .= "    $(this).html( '<input type=\'text\' placeholder=\'Search...\' />' );";
				$data .= "} );";
		      /*Initialize Datatable*/
		        if (is_array($_id)) {
		        $data .= "$('#".$table."').DataTable({'lengthChange':false,'data':".$this->get_data_tabel_json($table,$_id).",";
		    	}else{
		    	$data .= "$('#".$table."').DataTable({'lengthChange':false,'data':".$this->get_data_tabel_json($table).",";	
		    	}
		      /*End Initialize Datatable*/

		      /*Read Column Datatable*/
		        $data .= "columns: [";
		        $data .= "{'data':null},";
		        foreach ($query as $val2) {
			        $name2[] = $val2->Field;
			        $r_name2[$val2->Field] = $val2->Field;
			        $filter_col2 = array_diff($name2, $col);
			        $rename_col2 = array_intersect_key($_col, $r_name2);

			        if (in_array($val2->Field, $filter_col2)) {
			            if (in_array($val2->Field, array_keys($rename_col2))) {
			                $data .= "{ 'data': '".$val2->Field."'},";
			            } else {
			                $data .= "{ 'data': '".$val2->Field."'},";
			            }
			        }
			        
			    }
		    	$data .= "],";
			  /*End Read Column Datatable*/

			  /*Costumize Result Column*/			
		    	$data .= "columnDefs: [{ 'targets': [0], 'visible': true,";
		    	if(is_array($_id)){
		    	$data .= "'render': function ( data, type, row, meta ) {";
		    	$k_1 = array_keys($_id);
		    	$data .= "return";
		    	$data .= "'<a href=".current_url().'/edit/'."'+data.".$k_1[0]."+'/'+data.".$k_1[1]."+' class=\'btn btn-warning btn-sm\'>Edit</a>'";
		    	$data .= "<span style=\'padding: 0px 3px 0px 3px;\'></span>";
		    	$data .= "<a href=".current_url().'/delete/'."'+data.".$k_1[0]."+'/'+data.".$k_1[1]."+' class=\'btn btn-danger btn-sm\'>Hapus</a>'";
		    	$data .= "}";
		    	}else{
		    	$data .= "'render': function ( data, type, row, meta ) {";
				$data .= "return";
				$data .= "'<a href=".current_url().'/edit/'."'+data.".$_id."+' class=\'btn btn-warning btn-sm\'>Edit</a>";
				$data .= "<span style=\'padding: 0px 3px 0px 3px;\'></span>";
				$data .= "<a href=".current_url().'/delete/'."'+data.".$_id."+' class=\'btn btn-danger btn-sm\' onclick=\'return confirm(`Yakin Hapus Data Ini ?`);\'>Hapus</a>'";
				$data .= "}";
				}
		    	$data .= "},";
		    	$data .= "],";
		      /*End Costumize Result Column*/

		      /*Search by Column on footer table*/
		    	$data .= "initComplete: function () {";
				$data .= "    this.api().columns().every( function () {";
				$data .= "        var that = this;";
				$data .= "        $( 'input', this.footer() ).on( 'keyup change clear', function () {";
				$data .= "            if ( that.search() !== this.value ) {";
				$data .= "                that";
				$data .= "                    .search( this.value )";
				$data .= "                    .draw();";
				$data .= "            }";
				$data .= "        } );";
				$data .= "    } );";
			  /*End Search by Column*/

				$data .= "},'deferRender':true";

			  /*End Datatable*/
		    	$data .= "})";
		      /*End ready document jquery*/
		        $data .= "})";
		        $data .= "</script>";
		    }

		    /*Start Footer If Not using datatable*/
		    else {
		        $data .= "<div class='container-fluid'>";
		        $data .= "<div class='pull-left'>";
		        $data .= "<button class='btn btn-sm btn-success'> Total : ".$this->ci->db->count_all_results($table)."</button>";
		        $data .= "</div><div class='pull-right'>".$this->pagination($table)."</div></div>";
		    }
		    /*End Footer If Not using datatable*/

		    return $data;
		}

		/*
		| Pagination used when bambangcrud not using datatable
		| Recomended using serverside pagination, limit, offset.
		*/
		public function pagination($tbl)
		{
		    $no = 1;
		    $this->ci =& get_instance();
		    $config['page_query_string'] = FALSE;
		    $config['base_url'] = base_url($this->ci->uri->segment(1)).'?page='.++$no;
		    $config['total_rows'] = $this->ci->db->count_all_results($tbl);
		    $config['per_page'] = 10;
		    $config['num_links'] = 3;
		    $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
		    $config['full_tag_close'] = '</ul>';
		    
		    $this->ci->pagination->initialize($config);
		    
		    $hal = $this->ci->pagination->create_links();
		    return $hal;
		}

		/*
		| Method get data from database for using datatable ajax
		| has parsing string when data from database not clean "\n,\r,\s,\t,<,|,>"
		| recomended use deferrender datatable
		*/
		private function get_data_tabel_json($tbl,$_id=null)
		{
			$this->ci =& get_instance();$res=[];
			$query = ($_id == null) ? $this->ci->db->get($tbl)->result_array() : $this->ci->db->where($_id)->get($tbl)->result_array();
			foreach ($query as $key => $value) {
				$res[] = preg_replace('/([\r\n\t<|>])/', '', $value);
			}
			return json_encode($res);
		}

		public function menu_($kategori)
		{
			$this->ci =& get_instance();$menu = "";
			$query = $this->ci->db->get_where('menu',['kategori'=>$kategori])->result();
			foreach ($query as $key => $value) {
				$menu .= "<li><a href='".base_url($value->nama_menu)."'>".$value->nama_menu."</a></li>";
			}
			return $menu;
		}
	}
	
	/* End of file bambangcrud.php */
	/* Location: ./application/libraries/bambangcrud.php */
	
 ?>