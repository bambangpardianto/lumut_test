<nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="img/profile_small.jpg" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle">
                            
                        </div>
                        <div class="logo-element">
                            BP
                        </div>
                    </li>
                    <li>
                        <a href="<?=base_url('akun')?>"><i class="fa fa-flask"></i> <span class="nav-label">Akun</span></a>
                    </li>
                    <li>
                        <a href="<?=base_url('post')?>"><i class="fa fa-flask"></i> <span class="nav-label">Post</span></a>
                    </li>
                    <li>
                        <a href="<?=base_url('auth/logout')?>"><i class="fa fa-flask"></i> <span class="nav-label">Logout</span></a>
                    </li>
                    
                </ul>

            </div>
        </nav>