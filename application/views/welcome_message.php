<?php $this->load->view('includes/file_header'); ?>
<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div style="padding-top: 15rem;">
<h2>Login Lumut.id</h2>
<p>Admin</p>

<div id="infoMessage"></div>

<?php echo form_open("auth/login", 'class="form-horizontal"');?>

  <div class="form-group">
    <label class="col-sm-3 control-label">Username</label>
    <div class="col-sm-9">
      <?php echo form_input('username', '', 'class="form-control"');?>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label">Password</label>
    <div class="col-sm-9">
      <?php echo form_password('password', '', 'class="form-control"');?>
    </div>
  </div>

  <p><?php echo form_submit('submit', 'Masuk', 'class="btn btn-success"');?></p>

<?php echo form_close();?>


</div>
</div>

<?php $this->load->view('includes/file_footer'); ?>